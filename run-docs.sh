#! /usr/bin/bash

#Need to base64 encode images as pdoc creates all in one HTML files.
png='data:image/png;base64,'
fav=$(base64 js/cadorchestrator-app/public/static/images/favicon.png)
logo=$(base64 js/cadorchestrator-app/public/static/images/CadOrchestrator.png)


while getopts "hs" arg; do
  case $arg in
    h)
      echo "Generate the CadOrchestrator documentation"
      echo
      echo "Documentation will output to public"
      echo
      echo "-s : Serve documentation rather than generate. This won't serve the schema docs!"
      exit 0
      ;;
    s)
      pdoc --template-dir docs/_template/ --logo "${png}${logo}" --favicon "${png}${fav}" cadorchestrator/ !cadorchestrator.server
      exit 0
      ;;
    ?)
      exit 1
      ;;
  esac
done

pdoc --template-dir docs/_template/ --logo "${png}${logo}" --favicon "${png}${fav}" -o public cadorchestrator/ !cadorchestrator.server
cd public
generate-schema-doc --minify ../cadorchestrator/ConfigOptionsSchema.json
