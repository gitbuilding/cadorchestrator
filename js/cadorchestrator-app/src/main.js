import { createApp } from "vue";
import "./style.css";
import "./skeleton.css";
import App from "./App.vue";

import("@google/model-viewer");

createApp(App).mount("#app");
