# CadOrchestrator configuration App

This is a Vue3 JS app using Vite

Useful links:

- https://daily.dev/blog/start-vue-3-project-initial-steps
- https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup
- https://vuejs.org/guide/scaling-up/tooling.html#ide-support

## Installing

**Note:** This has been tested to work with Node v20. For managing node versions on UNIX systems consider using [NVM](https://github.com/nvm-sh/nvm)

Install the dependencies with:

    npm install

## Building

To build the vue application that will be served by `cadorchestrator serve` run:

    npm run build

## Dev server

During development of the vue app it is easier to run the development server. To do this run:

    npm run dev

This will start a dev server that serves the vue app and automatically updates and reloads as the vue code is modified. This should be hosted on: `http://localhost:5173/`.

For the app to function the back end CadOrchestrator server must also be running.

Go to an example project and run:

    cadorchestrator serve

**Note:** If you visit `http://127.0.0.1:8000` the server will still serve be the compiled vue app. This allows you to side-by-side test the compiled and the in development app in two browser tabs.
