## 0.1.0

* Add schema for configuration options
* Add more configuration options than just list (numeric, text, checkbox)
* Each configuration option has a key to identify it in the returned JSON
* Configurations are hashed for unique name rather than simply a list of all items
(as the configuration is now much more complex)
* Add these options to the WebApp
* Add a warning message to the WebApp that shows if there is a problem
* Add new Python exception that can be used to pass errors to the WebApp
* Catch any exceptions during running Generate and forward them to the WebApp
* Improve documentation

## 0.0.3

* Add dockerfile
* Add ability to use host 0.0.0.0
* Add check for if OS is headless

## 0.0.2

* Ability to add renders to components

## 0.0.1

* Switching the server to using Vue
* Creating entry point for CLI operation
* Removing nimble specific code and replacing with an interface that can be used by other projects

## First commit

The first commit contained code taken from the orchestration scripts held within the nimble repository (https://github.com/Wakoma/nimble). This code was written by
Andreas Kahler, Julian Stirling and Jeremy Wright
