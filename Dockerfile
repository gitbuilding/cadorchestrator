FROM python:3.12
COPY . .
RUN apt-get update -qq
RUN apt-get -o DPkg::Options::=--force-confdef -y -qq install openscad xvfb
RUN pip install -e .
RUN pip install scad2gltf
EXPOSE 8000
WORKDIR /data
ENTRYPOINT ["cadorchestrator", "serve", "--production"]

