![](assets/CadOrchestrator.png)

# CadOrchestrator

CadOrchestrator helps you create consistent production files and clear assembly documentation for your modular hardware project.

Modular hardware projects seem almost impossible to document. Writing separate assembly documentation and generating images of each assembly step for every possible combination of components seems like an infinite task. Rather than manually generating each permutation, CadOrchestrator will generate consistent documentation for a given configuration. CadOrchestrator provides a simple browser interface to configure the modular hardware, then using your parametric CAD models, assembly render descriptions, and documentation templates it will generate documentation specifically for any custom configuration. Configuration can also be generate from a CLI interface to integrate CadOrchestrator into custom workflows.

![](assets/UIexample.png)

CadOrchestrator is in an early development phase. We are focussing on creating documentation for the nimble. If you are interested in CadOrchestrator, please send us your feedback in an issue.

## Funding

This project is funded through [NGI Zero Core](https://nlnet.nl/core), a fund established by [NLnet](https://nlnet.nl) with financial support from the European Commission's [Next Generation Internet](https://ngi.eu) program. Learn more at the [NLnet project page](https://nlnet.nl/project/HardwareManuals).

[<img src="https://nlnet.nl/logo/banner.png" alt="NLnet foundation logo" width="20%" />](https://nlnet.nl)
[<img src="https://nlnet.nl/image/logos/NGI0_tag.svg" alt="NGI Zero Logo" width="20%" />](https://nlnet.nl/core)
