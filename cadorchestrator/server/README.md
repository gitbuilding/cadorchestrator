# CadOrchestrator Server

This is a server interface for CadOrchestrator that is installed with CadOrchestrator.

## Local Usage

To run the server locally:

1. navigate to your CAD project's directory.
2. Run:
```
cadorchestrator serve
```

The server will spin up a local web page used for testing

`http://127.0.0.1:8000/`

When the server is running you can access the API docs at:

`http://127.0.0.1:8000/docs`
